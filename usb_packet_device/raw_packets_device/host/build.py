import sys
import pdb

sys.path.insert(0, "../../../../python_library/make")


import make

profile_ut = make.profile_t()

include_dirs = ["."] + ["./../device"] + ["../../../../cpp_library/df"]

source_dirs = ["./"] + ["./tests"] + ["../../../../cpp_library/df"]

cflags = ["-g", "-Wall"]
ldflags = ["-g", "-Wall"]
libraries = ["-lusb-1.0"]

profile_ut.name = "ut"
profile_ut.cc = "gcc"
profile_ut.cc_asm = "gcc"
profile_ut.cxx = "g++"
profile_ut.ld = "g++"

profile_ut.include_dirs = include_dirs
profile_ut.source_dirs = source_dirs

profile_ut.cflags = cflags
profile_ut.cxxflags = cflags
profile_ut.ldflags = ldflags
profile_ut.libraries = libraries
profile_ut.executable = "ut.out"
profile_ut.build_dir = "./build"

profiles = [profile_ut]

make.build(profiles_list = profiles, args = sys.argv[1:])

