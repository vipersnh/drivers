import usb.core
import ctypes
from time import sleep

USB_COMM_INTF_MAX_NUM_PACKETS = 10
USB_COMM_INTF_MAX_PACKET_SIZE = 64

USB_COMM_DATAIN_EP  = 0x81
USB_COMM_DATAOUT_EP = 0x01

USB_COMM_DATA    = 0
USB_COMM_COMMAND = 1

USB_COMM_CMD_GET_TX_FIFO_STATS  = 0
USB_COMM_CMD_GET_RX_FIFO_STATS  = 1
USB_COMM_CMD_GET_TX_NEXT_PACKET = 2


class usb_comm_hdr_t(ctypes.Structure):
    _fields_ = [
            ("pkt_type", ctypes.c_uint8, 1),
            ("ch_id", ctypes.c_uint8, 3),
            ("length", ctypes.c_uint8, 4),
        ]

class usb_comm_packet_t(ctypes.Structure):
    _fields_ = [
            ("hdr", usb_comm_hdr_t),
            ("data", ctypes.c_uint8*(64-1))
        ]

class _fifo_stats_t(ctypes.Structure):
    _fields_ = [
            ("num_filled", ctypes.c_uint8),
            ("num_free", ctypes.c_uint8),
        ]

class _resp_data_t(ctypes.Union):
    _fields_ = [
        ("tx_fifo_stats", _fifo_stats_t),
        ("rx_fifo_stats", _fifo_stats_t),
    ]

class usb_comm_cmd_resp_t(ctypes.Structure):
    _fields_ = [
        ("cmd", ctypes.c_uint32),
        ("data", _resp_data_t),
    ]

class usb_comm_t:
    def usb_comm_t(self):
        pass
    
    def usb_comm_init(self, vid, pid):
        self.dev = usb.core.find(idVendor=vid, idProduct=pid)
        return self.usb_comm_connected()

    def usb_comm_connected(self):
        if self.dev:
            return True
        else:
            return False

    
    def _usb_comm_write(self, data):
        sleep(1/1000)
        self.dev.write(USB_COMM_DATAOUT_EP, data)
        self.dev.write(USB_COMM_DATAOUT_EP, [])
        return None
    
    def _usb_comm_read(self, size):
        sleep(1/1000)
        raw_resp = self.dev.read(USB_COMM_DATAIN_EP, size)
        return raw_resp

    def usb_comm_get_tx_fifo_stats(self):
        packet = usb_comm_packet_t()
        packet.hdr.pkt_type = USB_COMM_COMMAND
        packet.hdr.ch_id    = 0
        packet.hdr.length   = 5
        packet.data[0] = USB_COMM_CMD_GET_TX_FIFO_STATS
        self._usb_comm_write(bytearray(packet))
        raw_resp = self._usb_comm_read(6)
        raw_resp.append(0)  # For padding
        raw_resp.append(0)  # For padding
        resp = usb_comm_cmd_resp_t.from_buffer(raw_resp)
        assert resp.cmd==USB_COMM_CMD_GET_TX_FIFO_STATS
        return (resp.data.tx_fifo_stats.num_filled, resp.data.tx_fifo_stats.num_free)

    def usb_comm_get_rx_fifo_stats(self):
        packet = usb_comm_packet_t()
        packet.hdr.pkt_type = USB_COMM_COMMAND
        packet.hdr.ch_id    = 0
        packet.hdr.length   = 5
        packet.data[0] = USB_COMM_CMD_GET_RX_FIFO_STATS
        self._usb_comm_write(bytearray(packet))
        raw_resp = self._usb_comm_read(6)
        raw_resp.append(0)  # For padding
        raw_resp.append(0)  # For padding
        resp = usb_comm_cmd_resp_t.from_buffer(raw_resp)
        assert resp.cmd==USB_COMM_CMD_GET_RX_FIFO_STATS
        return (resp.data.rx_fifo_stats.num_filled, resp.data.rx_fifo_stats.num_free)


    def usb_comm_receive_packet(self):
        packet = usb_comm_packet_t()
        packet.hdr.pkt_type = USB_COMM_COMMAND
        packet.hdr.ch_id    = 0
        packet.hdr.length   = 5
        packet.data[0] = USB_COMM_CMD_GET_TX_NEXT_PACKET
        self._usb_comm_write(bytearray(packet))
        raw_resp = self._usb_comm_read(USB_COMM_INTF_MAX_PACKET_SIZE)
        packet = usb_comm_packet_t.from_buffer(raw_resp)
        return (packet.hdr, packet.data)

    def usb_comm_send_packet(self, packet_data, ch_id):
        assert(len(packet_data) <= 63)
        packet = usb_comm_packet_t()
        packet.hdr.pkt_type = USB_COMM_DATA
        packet.hdr.ch_id = ch_id
        packet.hdr.length = 1 + len(packet_data)
        packet.data = (ctypes.c_uint8*(64-1))(*packet_data)
        self._usb_comm_write(bytearray(packet))
        return None

