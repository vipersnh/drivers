#ifndef _USB_COMM_INTF_HOST_HPP_
#define _USB_COMM_INTF_HOST_HPP_


class usb_comm_t {
    public:
        usb_comm_t(void);
        static usb_comm_t * get_instance();
        bool usb_comm_init(uint16_t vid, uint16_t pid);
        bool usb_comm_get_tx_fifo_stats(uint32_t * num_filled, uint32_t * num_free);
        bool usb_comm_get_rx_fifo_stats(uint32_t * num_filled, uint32_t * num_free);
        bool usb_comm_receive_packet(uint8_t * ch_id, uint8_t * data, uint8_t max_len);
        bool usb_comm_send_packet(uint8_t ch_id, uint8_t * data, uint8_t len);

    protected:
        /* LibUSB device handle and context */
        libusb_context       *lusb_ctxt;
        libusb_device_handle *lusb_dev_handle;

        /* Statistics */
        uint32_t num_packets_sent;
        uint32_t num_packets_recieved;

    private:
        static usb_comm_t * instance;
};

extern usb_comm_t * g_usb_comm_handle;

#endif
