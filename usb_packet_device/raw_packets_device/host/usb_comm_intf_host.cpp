extern "C" {
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <unistd.h>
    #include <libusb-1.0/libusb.h>
}

#include "df_base.h"
#include "usb_comm_intf.hpp"
#include "usb_comm_intf_host.hpp"

usb_comm_t * g_usb_comm_handle = NULL;

usb_comm_t * usb_comm_t::instance;

usb_comm_t::usb_comm_t(void)
{
    lusb_ctxt               = NULL;
    lusb_dev_handle         = NULL;
    num_packets_sent        = 0;
    num_packets_recieved    = 0;
    instance = NULL;
}

usb_comm_t * usb_comm_t::get_instance()
{
    if (instance==NULL) {
        instance = new usb_comm_t();
    } else {
        /* TODO */
    }
    g_usb_comm_handle = instance;
    return instance;
}

bool usb_comm_t::usb_comm_init(uint16_t vid, uint16_t pid)
{
    int status = 0;
    status = libusb_init(&lusb_ctxt);
    if(status < 0) {
        printf("Libusb context initialization failed\n");
        return false;
    }
    lusb_dev_handle = libusb_open_device_with_vid_pid(lusb_ctxt, vid, pid);
    if (lusb_dev_handle==NULL) {
        printf("Please connect the usb device\n");
        exit(0);
        return false;
    } else {
        status = libusb_claim_interface(lusb_dev_handle, 0);
        if (status<0) {
            printf("Interface claiming failed\n");
            return false;
        }
    }
    return true;
}

bool usb_comm_t::usb_comm_get_tx_fifo_stats(uint32_t * num_filled, uint32_t * num_free)
{
    if (num_filled && num_free) {
        int status = 0;
        int num_bytes = 0;
        usb_comm_cmd_resp_t resp = {0};
        usb_comm_packet_t packet = {0}; 
        usb_comm_cmd_t cmd;
        packet.hdr.pkt_type = USB_COMM_COMMAND;
        packet.hdr.ch_id = 0;
        packet.hdr.length = sizeof(usb_comm_intf_hdr_t) + sizeof(usb_comm_cmd_t);
        cmd.cmd = USB_COMM_CMD_GET_TX_FIFO_STATS;
        memcpy(packet.data, &cmd, sizeof(usb_comm_cmd_t));
        status = libusb_bulk_transfer(lusb_dev_handle, USB_COMM_DATAOUT_EP, (unsigned char*)&packet, 
            sizeof(packet), &num_bytes, 0);
        if (status!=0) {
            return false;
        }
        status = libusb_bulk_transfer(lusb_dev_handle, USB_COMM_DATAOUT_EP, NULL, 
            0, &num_bytes, 0);
        status = libusb_bulk_transfer(lusb_dev_handle, USB_COMM_DATAIN_EP, (unsigned char*)&resp,
            sizeof(usb_comm_cmd_resp_t), &num_bytes, 0);
        if (status!=0) {
            return false;
        }   
        *num_filled = resp.data.tx_fifo_stats.num_filled;
        *num_free = resp.data.tx_fifo_stats.num_free;
        return true;
    }
    return false;
}

bool usb_comm_t::usb_comm_get_rx_fifo_stats(uint32_t * num_filled, uint32_t * num_free)
{
    if (num_filled && num_free) {
        int status = 0;
        int num_bytes = 0;
        usb_comm_cmd_resp_t resp = {0};
        usb_comm_packet_t packet = {0}; 
        usb_comm_cmd_t cmd;
        packet.hdr.pkt_type = USB_COMM_COMMAND;
        packet.hdr.ch_id = 0;
        packet.hdr.length = sizeof(usb_comm_intf_hdr_t) + sizeof(usb_comm_cmd_t);
        cmd.cmd = USB_COMM_CMD_GET_RX_FIFO_STATS;
        memcpy(packet.data, &cmd, sizeof(usb_comm_cmd_t));
        status = libusb_bulk_transfer(lusb_dev_handle, USB_COMM_DATAOUT_EP, (unsigned char*)&packet, 
            sizeof(packet), &num_bytes, 0);
        if (status!=0) {
            return false;
        }
        status = libusb_bulk_transfer(lusb_dev_handle, USB_COMM_DATAOUT_EP, NULL, 
            0, &num_bytes, 0);

        memset(&packet, 0x00, sizeof(packet));
        status = libusb_bulk_transfer(lusb_dev_handle, USB_COMM_DATAIN_EP, (unsigned char*)&resp,
            sizeof(usb_comm_cmd_resp_t), &num_bytes, 0);
        if (status!=0) {
            return false;
        }   
        *num_filled = resp.data.rx_fifo_stats.num_filled;
        *num_free = resp.data.rx_fifo_stats.num_free;
        return true;
    }
    return false;
}

bool usb_comm_t::usb_comm_receive_packet(uint8_t * ch_id, uint8_t * data, uint8_t max_len)
{
    if (ch_id && data && max_len) {
        {
            /* Wait till a packet is available for upto 5 seconds */
            uint32_t num_free, num_filled;
            uint32_t time = 0;
            bool status;
            while (1) {
                status = this->usb_comm_get_tx_fifo_stats(&num_filled, &num_free);
                df_assert(status);
                if (num_filled || time > 5000) {
                    break;
                } else {
                    time++;
                    usleep(100);
                }
            }
            if (!num_filled) {
                return false;
            } else {
                usleep(1);
            }
        }
        int status = 0;
        int num_bytes = 0;
        usb_comm_packet_t packet = {0};
        usb_comm_cmd_t cmd;
        packet.hdr.pkt_type = USB_COMM_COMMAND;
        packet.hdr.ch_id = 0;
        packet.hdr.length = sizeof(usb_comm_intf_hdr_t) + sizeof(usb_comm_cmd_t);
        cmd.cmd = USB_COMM_CMD_GET_TX_NEXT_PACKET;
        memcpy(packet.data, &cmd, sizeof(usb_comm_cmd_t));
        status = libusb_bulk_transfer(lusb_dev_handle, USB_COMM_DATAOUT_EP, (unsigned char*)&packet, 
            sizeof(packet), &num_bytes, 0);
        if (status!=0) {
            return false;
        }
        status = libusb_bulk_transfer(lusb_dev_handle, USB_COMM_DATAOUT_EP, NULL, 
            0, &num_bytes, 0);

        memset(&packet, 0x00, sizeof(packet));
        status = libusb_bulk_transfer(lusb_dev_handle, USB_COMM_DATAIN_EP, (unsigned char*)&packet,
            USB_COMM_INTF_MAX_PACKET_SIZE, &num_bytes, 0);
        if (status!=0) {
            return false;
        }
        *ch_id = packet.hdr.ch_id;
        memcpy(data, packet.data, df_min(packet.hdr.length-sizeof(usb_comm_intf_hdr_t), max_len));
        return true;
    } else {
        return false;
    }
}


bool usb_comm_t::usb_comm_send_packet(uint8_t ch_id, uint8_t * data, uint8_t len)
{
    if (data && len) {
        {
            /* Wait till a slot is available for upto 5 seconds */
            uint32_t num_free, num_filled;
            uint32_t time = 0;
            bool status;
            while (1) {
                status = this->usb_comm_get_rx_fifo_stats(&num_filled, &num_free);
                df_assert(status);
                if (num_free || time > 5000) {
                    break;
                } else {
                    time++;
                    usleep(100);
                }
            }
            if (!num_free) {
                return false;
            } else {
                usleep(1);
            }
        }
        int status = 0;
        int num_bytes = 0;
        usb_comm_packet_t packet = {0};
        packet.hdr.pkt_type = USB_COMM_DATA;
        packet.hdr.ch_id = ch_id;
        packet.hdr.length = sizeof(usb_comm_intf_hdr_t) + len;
        memcpy(packet.data, data, len);
        status = libusb_bulk_transfer(lusb_dev_handle, USB_COMM_DATAOUT_EP, (unsigned char*)&packet, 
            sizeof(packet), &num_bytes, 0);
        if (status!=0) {
            return false;
        } 
        status = libusb_bulk_transfer(lusb_dev_handle, USB_COMM_DATAOUT_EP, NULL, 
            0, &num_bytes, 0);
        if (status!=0) {
            return false;
        } 
        return true;
    } 
    return false;
}
