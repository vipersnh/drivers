
# Test Sequence
#   1. Send get tx fifo stats
#   2. Send get rx fifo stats
#   3. Send 10 tx data packets
#   4. Get 10 tx packets back via rx packets

import os
import sys
import datetime
import pdb
from time import sleep
from random import randint
from binascii import hexlify
from usb_comm_intf_host import usb_comm_t

idVendor=0xf0f0
idProduct=0xa0b0

usb_comm = usb_comm_t()
usb_comm.usb_comm_init(idVendor, idProduct)
if usb_comm.dev==None:
    print("Please connect the usb ")
    exit()

# Step 1.
(num_filled, num_free) = usb_comm.usb_comm_get_tx_fifo_stats()
assert num_filled==0
assert num_free>0

# Step 2.
(num_filled, num_free) = usb_comm.usb_comm_get_rx_fifo_stats()
assert num_filled==0
assert num_free>0


for x in range(1000000000):
    # Step 3.
    sent_data_list = []
    packet_data = [0xAB]*30
    start = datetime.datetime.now()
    num_bytes_out = 0
    num_bytes_in = 0
    print("Sending packets")
    for i in range(10):
        ch_id = randint(0,7)
        packet_data = [randint(0,255) for i in range(randint(50, 63))]
        sent_data_list.append((ch_id, packet_data))
        print(hexlify(bytearray(packet_data)).decode()+" of length {0}".format(len(packet_data)))
        usb_comm.usb_comm_send_packet(packet_data, ch_id)
        num_bytes_out += len(packet_data)
    
    sleep(1/100)
    (num_filled, num_free) = usb_comm.usb_comm_get_rx_fifo_stats()
    assert num_filled==0
    assert num_free==10


    # Step 4.
    print("Recieving packets")
    for i in range(10):
        (ch_id_sent, packet_data_sent) = sent_data_list[i]
        (packet_hdr, packet_data) = usb_comm.usb_comm_receive_packet()
        assert packet_hdr.ch_id==ch_id_sent
        assert packet_data_sent==packet_data[0:len(packet_data_sent)]
        print(hexlify(bytearray(packet_data)).decode()+" of length {0}".format(len(packet_data)))
        num_bytes_in += len(packet_data)
 
    sleep(1/100)
    (num_filled, num_free) = usb_comm.usb_comm_get_tx_fifo_stats()
    assert num_filled==0
    assert num_free==10

    os.system('clear')

    end = datetime.datetime.now()
    delta = (end-start).microseconds
    delta /= 1E6
    print("USB Transfer speed : OUT = {0:0.2f}KBps, IN = {1:0.2f}KBps".format(num_bytes_out/1024/delta, num_bytes_in/1024/delta))
    sys.stdout.flush()

print("All done.. Verify")
