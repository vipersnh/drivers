extern "C" {
    #include <stdio.h>
    #include <stdlib.h>
    #include <pthread.h>
    #include <unistd.h>
}

#include "df_base.h"

#include <libusb-1.0/libusb.h>
#include "usb_comm_intf.hpp"
#include "usb_comm_intf_host.hpp"

#define ut_assert(_x) { \
    if (!(_x)) { \
        printf("Assert failed\n"); \
        while (1) { sleep(1); } \
    } \
}


int main()
{
    uint32_t idx;
    uint8_t send_ch_ids[USB_COMM_INTF_MAX_NUM_PACKETS];
    uint8_t recv_ch_ids[USB_COMM_INTF_MAX_NUM_PACKETS];
    uint8_t send_data[USB_COMM_INTF_MAX_NUM_PACKETS][sizeof(usb_comm_packet_t)-sizeof(usb_comm_intf_hdr_t)];
    uint8_t recv_data[USB_COMM_INTF_MAX_NUM_PACKETS][sizeof(usb_comm_packet_t)-sizeof(usb_comm_intf_hdr_t)];
    uint32_t num_filled, num_free;
    bool status;
    for (uint8_t i=0; i<USB_COMM_INTF_MAX_NUM_PACKETS; i++) {
        send_ch_ids[i] = rand()%8;
        for (uint8_t j=0; j<sizeof(send_data[i]); j++) {
            send_data[i][j] = rand()%256;
        }
    }
    usb_comm_t * usb_comm_handle = NULL;
    usb_comm_handle = usb_comm_t::get_instance();
    usb_comm_handle->usb_comm_init(0xF0F0, 0xA0B0);

    /* Step 1 */
    status = usb_comm_handle->usb_comm_get_tx_fifo_stats(&num_filled, &num_free);
    ut_assert(status);
    ut_assert(num_filled==0);
    ut_assert(num_free==10);

    sleep(0.5);

    /* Step 2 */
    status = usb_comm_handle->usb_comm_get_rx_fifo_stats(&num_filled, &num_free);
    ut_assert(status);
    ut_assert(num_filled==0);
    ut_assert(num_free==10);

    uint32_t num_iter = 100;
    
    do {
        for (idx=0; idx<USB_COMM_INTF_MAX_NUM_PACKETS; idx++) {
            status = g_usb_comm_handle->usb_comm_send_packet(send_ch_ids[idx], 
                send_data[idx], sizeof(send_data[idx]));
            ut_assert(status);
            usleep(10);
        }
    
        usleep(1000);

        status = usb_comm_handle->usb_comm_get_rx_fifo_stats(&num_filled, &num_free);
        ut_assert(status);
        ut_assert(num_filled==0);
        ut_assert(num_free==10);

        for (idx=0; idx<USB_COMM_INTF_MAX_NUM_PACKETS; idx++) {
            status = g_usb_comm_handle->usb_comm_receive_packet(&recv_ch_ids[idx], recv_data[idx], 
                sizeof(recv_data[idx]));
            ut_assert(status);
            usleep(10);
        }

        usleep(1000);

        status = usb_comm_handle->usb_comm_get_tx_fifo_stats(&num_filled, &num_free);
        ut_assert(status);
        ut_assert(num_filled==0);
        ut_assert(num_free==10);

        for (idx=0; idx<USB_COMM_INTF_MAX_NUM_PACKETS; idx++) {
            ut_assert(send_ch_ids[idx]==recv_ch_ids[idx]);
            for (uint32_t byte_idx=0; byte_idx<sizeof(send_data[idx]); byte_idx++) {
                ut_assert(send_data[idx][byte_idx]==recv_data[idx][byte_idx]);
            }
        }
        printf("Iteration : %d\n", num_iter);
    } while (num_iter--);
    printf("UT Completed\n");
    return 0;
}
