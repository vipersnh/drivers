#ifndef __USB_CORE_COMMON_HPP_
#define __USB_CORE_COMMON_HPP_

#include "cbuf_packet.h"
#include "cbuf_char.h"

extern bool g_usb_assert_end;

extern void (*g_usb_assert_callback)(void);
extern "C" {
extern void usb_assert(bool);
}

#define _PACKED_    __attribute__((__packed__))


typedef struct {
    uint8_t lower_byte;
    uint8_t upper_byte;
} _PACKED_  usb_uint16_t;

#define USB_DEVICE_DESCRIPTOR_LENGTH        (18)
#define USB_ENDPOINT_DESCRIPTOR_LENGTH      (7)
#define USB_INTERFACE_DESCRIPTOR_LENGTH     (9)
#define USB_CONFIGURATION_DESCRIPTOR_LENGTH (9)

typedef enum {
    USB_DEVICE_ENDPOINT_CONTROL = 0x00,
    USB_DEVICE_ENDPOINT_ISOCHRONOUS = 0x01,
    USB_DEVICE_ENDPOINT_BULK = 0x02,
    USB_DEVICE_ENDPOINT_INTERRUPT = 0x03,
} usb_device_endpoint_type_enum_t;

typedef struct {
    struct {
        uint8_t  bLength;
        uint8_t  bDescriptorType;
        uint16_t bcdUSB;
        uint8_t  bDeviceClass;
        uint8_t  bDeviceSubClass;
        uint8_t  bDeviceProtocol;
        uint8_t  bMaxPacketSize0;
        uint16_t idVendor;
        uint16_t idProduct;
        uint16_t bcdDevice;
        uint8_t  iManufacturer;
        uint8_t  iProduct;
        uint8_t  iSerialNumber;
        uint8_t  bNumConfigurations;
    } _PACKED_ std;
} usb_device_descriptor_t;

typedef enum {
    USB_DEVICE_EP_STATE_IDLE,
    USB_DEVICE_EP_STATE_RECEIVE_STATUS,
    USB_DEVICE_EP_STATE_SEND_STATUS,
} usb_device_endpoint_state_enum_t;

typedef enum {
    USB_DEVICE_EP_TRANSACTION_IDLE,
    USB_DEVICE_EP_TRANSACTION_SCHEDULED,
} usb_device_transaction_state_enum_t;

typedef struct {
    struct {
        uint8_t  bLength;
        uint8_t  bDescriptorType;
        uint8_t  bEndpointAddress;
        uint8_t  bmAttributes;
        uint16_t wMaxPacketSize;
        uint8_t  bInterval;
    } _PACKED_ datain_std;
    struct {
        uint8_t  bLength;
        uint8_t  bDescriptorType;
        uint8_t  bEndpointAddress;
        uint8_t  bmAttributes;
        uint16_t wMaxPacketSize;
        uint8_t  bInterval;
    } _PACKED_ dataout_std;

    uint16_t                            datain_max_packet_size;
    uint16_t                            dataout_max_packet_size;
    cbuf_packet_t                       datain_packet_fifo;
    cbuf_packet_t                       dataout_packet_fifo;
    cbuf_char_t                         datain_byte_stream;
    cbuf_char_t                         dataout_byte_stream;
    usb_device_endpoint_state_enum_t    state;
    usb_device_transaction_state_enum_t datain_state;
    usb_device_transaction_state_enum_t dataout_state;

    /* For debug */
    uint32_t                            num_dataout;
    uint32_t                            num_datain;
} usb_device_endpoint_descriptor_t;

typedef struct {
    struct {
        uint8_t bLength;
        uint8_t bDescriptorType;
        uint8_t bInterfaceNumber;
        uint8_t bAlternateSetting;
        uint8_t bNumEndpoints;
        uint8_t bInterfaceClass;
        uint8_t bInterfaceSubClass;
        uint8_t bInterfaceProtocol;
        uint8_t iInterface;
    } _PACKED_ std;
    uint8_t                             num_endpoints;          /* Number of non-zero endpoints */
    usb_device_endpoint_descriptor_t  * endpoint_descriptors;   /* List of non-zero endpoints */
} usb_device_interface_descriptor_t;

typedef struct {
    struct {
        uint8_t  bLength;
        uint8_t  bDescriptorType;
        uint16_t wTotalLength;
        uint8_t  bNumInterfaces;
        uint8_t  bConfigurationValue;
        uint8_t  iConfiguration;
        uint8_t  bmAttributes;
        uint8_t  bMaxPower;
    } _PACKED_ std;
    uint8_t                             num_interfaces;
    usb_device_interface_descriptor_t * interface_descriptors;
} usb_device_configuration_descriptor_t;

#define USB_DEVICE_NUM_LANGID               1
#define USB_DEVICE_NUM_STRING_DESCRIPTORS   6
#define USB_DEVICE_STRING_DESCRIPTOR_LEN    100

typedef struct {
    struct {
        uint8_t     bLength;
        uint8_t     bDescriptorType;
        uint16_t    wLANGID[USB_DEVICE_NUM_LANGID];
    } _PACKED_ std;
    uint8_t         num_lang_id;
} usb_device_string_descriptor_zero_t;

typedef struct {
    struct {
        uint8_t     bLength;
        uint8_t     bDescriptorType;
        char        bString[USB_DEVICE_STRING_DESCRIPTOR_LEN];
    } _PACKED_ std;
} usb_device_string_descriptor_t;

typedef struct {
    uint8_t                                 device_addr;
    uint8_t                                 device_configuration_value;
    usb_device_descriptor_t               * device_descriptor;
    uint8_t                                 num_configurations;
    usb_device_configuration_descriptor_t * configuration_descriptors;
    usb_device_string_descriptor_zero_t     str_descriptor0;
    usb_device_string_descriptor_t          str_descriptors[USB_DEVICE_NUM_STRING_DESCRIPTORS];
    uint8_t                                 num_str_descriptors;
} usb_device_complete_descriptor_t;

#endif
