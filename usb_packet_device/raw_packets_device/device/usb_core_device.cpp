extern "C" {
    #include <stdint.h>
    #include <stdio.h>
    #include <string.h>
}
#include "cbuf_packet.h"
#include "usb_core_common.hpp"
#include "usb_core_device.hpp"

typedef struct {
    uint8_t data_recipient : 5;
    uint8_t request_type   : 2;
    uint8_t data_direction : 1;
} usb_setup_bmRequestType_t;

void (*g_usb_assert_callback)(void);
usb_device_handle_t * g_usb_device_handle;

usb_device_handle_t * usb_device_handle_t::instance;

usb_device_handle_t::usb_device_handle_t(void)
{
    uint8_t i;
    hw_handlers            = NULL;
    core_handlers          = NULL;
    state_handlers         = NULL;
    num_ep_flushing = 0;
    num_setup_packets_handled = 0;
    num_device_descriptor_reqs = 0;
    num_set_address_reqs = 0;
    instance->control_ep = &eps[0];
    memset(&this->descriptor, 0x00, sizeof(this->descriptor));
    usb_device_handle_init();
}

usb_device_handle_t * usb_device_handle_t::get_instance()
{
    if (instance==NULL) {
        instance = new usb_device_handle_t();
    } else {
        /* TODO */
    }
    g_usb_device_handle = instance;
    return instance;
}

void usb_device_handle_t::usb_device_handle_init()
{
    usb_device_init();
}

void usb_device_handle_t::usb_device_setup_descriptors(
            usb_device_descriptor_t * dev_descriptor, 
            usb_device_configuration_descriptor_t *conf_descriptor, 
            uint8_t num_configurations,
            usb_device_endpoint_descriptor_t * control_ep,
            usb_device_string_descriptor_zero_t * str_descriptor0,
            usb_device_string_descriptor_t  * str_descriptors,
            uint8_t num_str_descriptors)
{
    usb_assert(instance);
    instance->descriptor.device_addr = 0;
    instance->descriptor.device_configuration_value = 0;
    instance->descriptor.device_descriptor = dev_descriptor;
    instance->descriptor.num_configurations = num_configurations;
    instance->descriptor.configuration_descriptors = conf_descriptor;
    /* TODO setup eps pointers for other endpoints */
    instance->eps[0]  = *control_ep;

    for (uint8_t idx=0; idx<conf_descriptor->num_interfaces; idx++) {
        usb_device_interface_descriptor_t * intf = conf_descriptor->interface_descriptors + idx;
        usb_assert((intf->num_endpoints%2)==0);
        for (uint8_t ep_idx=1; ep_idx<=intf->num_endpoints/2; ep_idx++) {
            uint8_t ep_addr = intf->endpoint_descriptors[ep_idx].dataout_std.bEndpointAddress;
            eps[ep_addr] = intf->endpoint_descriptors[ep_idx];
        }
    }

    instance->descriptor.str_descriptor0 = *str_descriptor0;
    instance->descriptor.num_str_descriptors = num_str_descriptors;
    for (uint8_t idx=0; idx<df_min(num_str_descriptors, USB_DEVICE_NUM_STRING_DESCRIPTORS); idx++) {
        instance->descriptor.str_descriptors[idx] = str_descriptors[idx];
    }
}


void usb_device_handle_t::usb_device_setup_core_handlers(usb_device_core_handlers_t * core_handlers)
{
  this->core_handlers = core_handlers;
}

void usb_device_handle_t::usb_device_setup_hardware_handlers(usb_device_hardware_handlers_t * hw_handlers)
{
    this->hw_handlers = hw_handlers;
}

void usb_device_handle_t::usb_device_setup_state_handlers(usb_device_state_handlers_t * state_handlers)
{
    this->state_handlers = state_handlers;
}

void usb_device_handle_t::usb_device_handle_setup_request(usb_setup_packet_t * request, uint8_t ep_num)
{
    usb_device_endpoint_descriptor_t * ep = usb_device_get_ep_descriptor(ep_num);
    usb_assert(ep);
    usb_setup_bmRequestType_t * bmRequestType = (usb_setup_bmRequestType_t *)&request->std.bmRequest;
    uint8_t flat_buffer[256], flat_buffer_len;
    uint8_t bRequest = request->std.bRequest;
    bool send_ctrl_info = false;
    bool status = false;
    switch (bmRequestType->request_type) {
        case USB_STANDARD_REQUEST:
            switch (bmRequestType->data_recipient) {
                case USB_DEVICE:
                    switch (bRequest) {
                        case USB_DEVICE_GET_STATUS        :
                            send_ctrl_info = true;
                            flat_buffer_len = 2;
                            flat_buffer[0] = 0x00;
                            flat_buffer[1] = 0x00;
                            ep->datain_packet_fifo.cbuf_push_packet_buf(flat_buffer, flat_buffer_len);
                            break;
                        case USB_DEVICE_CLEAR_FEATURE     :
                            usb_assert(0);
                            break;
                        case USB_DEVICE_SET_FEATURE       :
                            usb_assert(0);
                            break;
                        case USB_DEVICE_SET_ADDRESS       :
                            DF_DEBUG_NEXT_EVENT(USB_DEVICE_SET_DEVICE_ADDRESS);
                            ep->state = USB_DEVICE_EP_STATE_SEND_STATUS;
                            DF_DEBUG_NEXT_EVENT(USB_DEVICE_EP0_EP_STATE_SEND_STATUS);
                            descriptor.device_addr = request->std.wValue;
                            hw_handlers->set_address(descriptor.device_addr);
                            ep->datain_packet_fifo.cbuf_push_packet_buf(NULL, 0); 
                            usb_device_set_next_state(USB_DEVICE_STATE_ADDRESSED);
                            send_ctrl_info = true;
                            break;
                        case USB_DEVICE_GET_DESCRIPTOR    :
                        {
                            DF_DEBUG_NEXT_EVENT(USB_DEVICE_GET_DEVICE_DESCRIPTOR);
                            uint8_t descriptor_idx = ((usb_uint16_t *) &request->std.wValue)->lower_byte;
                            uint8_t descriptor_type = ((usb_uint16_t *) &request->std.wValue)->upper_byte;
                            num_device_descriptor_reqs++;
                            send_ctrl_info = true;
                            ep->state = USB_DEVICE_EP_STATE_RECEIVE_STATUS;
                            DF_DEBUG_NEXT_EVENT(USB_DEVICE_EP0_EP_STATE_RECEIVE_STATUS);
                            switch (descriptor_type) {
                                case USB_DEVICE_DESCRIPTOR        :
                                    for (int idx=0; idx<USB_DEVICE_DESCRIPTOR_LENGTH; idx++) {
                                        flat_buffer[idx] = *((uint8_t *)&this->descriptor.device_descriptor->std + idx);
                                    }
                                    if (usb_device_get_curr_state()==USB_DEVICE_STATE_DEFAULT) {
                                        flat_buffer_len = 8;
                                    } else {
                                        flat_buffer_len = request->std.wLength;
                                    }
                                    status = ep->datain_packet_fifo.cbuf_push_packet_buf(flat_buffer, flat_buffer_len); 
                                    usb_assert(status);
                                    break;
                                case USB_CONFIGURATION_DESCRIPTOR :
                                    {
                                        flat_buffer_len = 0;
                                        for (uint8_t idx=0; idx<USB_CONFIGURATION_DESCRIPTOR_LENGTH; idx++) {
                                            flat_buffer[flat_buffer_len++] = *((uint8_t*)&this->descriptor.configuration_descriptors->std + idx);
                                        }

                                        for (uint8_t idx=0; idx<USB_INTERFACE_DESCRIPTOR_LENGTH; idx++) {
                                            flat_buffer[flat_buffer_len++] = *((uint8_t*)&this->descriptor.configuration_descriptors->interface_descriptors[0].std + idx);
                                        }

                                        for (uint8_t idx=0; idx<USB_ENDPOINT_DESCRIPTOR_LENGTH; idx++) {
                                            flat_buffer[flat_buffer_len++] = 
                                                *((uint8_t*)&this->descriptor.configuration_descriptors->interface_descriptors[0].endpoint_descriptors[1].datain_std+idx);
                                        }
                                        for (uint8_t idx=0; idx<USB_ENDPOINT_DESCRIPTOR_LENGTH; idx++) {
                                            flat_buffer[flat_buffer_len++] = 
                                                *((uint8_t*)&this->descriptor.configuration_descriptors->interface_descriptors[0].endpoint_descriptors[1].dataout_std+idx);
                                        }

                                        if (request->std.wLength==USB_CONFIGURATION_DESCRIPTOR_LENGTH) {
                                            /* Return only configuration descriptor */
                                            flat_buffer_len = USB_CONFIGURATION_DESCRIPTOR_LENGTH;
                                        } else if (request->std.wLength==(USB_CONFIGURATION_DESCRIPTOR_LENGTH + USB_INTERFACE_DESCRIPTOR_LENGTH)) {
                                            /* Return configuration descriptor and endpoint descriptor */
                                            flat_buffer_len = USB_CONFIGURATION_DESCRIPTOR_LENGTH + USB_INTERFACE_DESCRIPTOR_LENGTH;
                                        } else if (request->std.wLength==(USB_CONFIGURATION_DESCRIPTOR_LENGTH + USB_INTERFACE_DESCRIPTOR_LENGTH + 2*USB_ENDPOINT_DESCRIPTOR_LENGTH)) {
                                            flat_buffer_len = USB_CONFIGURATION_DESCRIPTOR_LENGTH + USB_INTERFACE_DESCRIPTOR_LENGTH + 2*USB_ENDPOINT_DESCRIPTOR_LENGTH;
                                        } else {
                                            usb_assert(0);
                                        }
                                        status = ep->datain_packet_fifo.cbuf_push_packet_buf(flat_buffer, flat_buffer_len);
                                        usb_assert(status);
                                    }
                                    break;
                                case USB_STRING_DESCRIPTOR        :
                                    {
                                        if (descriptor_idx==0) {
                                            send_ctrl_info = true;
                                            usb_device_string_descriptor_zero_t * str_descriptor0 = 
                                                &this->descriptor.str_descriptor0;
                                            /* Return the string descriptor zero */
                                            for (int idx=0; idx<str_descriptor0->std.bLength; idx++) {
                                                flat_buffer[idx] = *((uint8_t*)&str_descriptor0->std + idx);
                                            }
                                            flat_buffer_len = str_descriptor0->std.bLength;
                                        } else {
                                            if (descriptor_idx <= this->descriptor.num_str_descriptors) {
                                                send_ctrl_info = true;
                                                usb_device_string_descriptor_t * str_descriptor = 
                                                    &this->descriptor.str_descriptors[descriptor_idx-1];
                                                /* Return the indexed string descriptor */
                                                for (int idx=0; idx<str_descriptor->std.bLength; idx++) {
                                                    flat_buffer[idx] = *((uint8_t*)&str_descriptor->std + idx);
                                                }
                                                flat_buffer_len = str_descriptor->std.bLength;
                                            } else {
                                                usb_assert(0);
                                            }
                                        }
                                        status = ep->datain_packet_fifo.cbuf_push_packet_buf(flat_buffer, flat_buffer_len);
                                        usb_assert(status);
                                    }
                                    break;
                                case USB_DEBUG_DESCRIPTOR:
                                    send_ctrl_info = true;
                                    ep->datain_packet_fifo.cbuf_push_packet_buf(NULL, 0);
                                    break;
                                case USB_INTERFACE_DESCRIPTOR     :
                                case USB_ENDPOINT_DESCRIPTOR      :
                                default:
                                    usb_assert(0);
                                    break;
                            }
                        } break;
                        case USB_DEVICE_SET_DESCRIPTOR    :
                            usb_assert(0);
                            /* Ignored */
                            break;
                        case USB_DEVICE_GET_CONFIGURATION :
                            usb_assert(0);
                            break;
                        case USB_DEVICE_SET_CONFIGURATION :
                            DF_DEBUG_NEXT_EVENT(USB_DEVICE_SET_DEVICE_CONFIGURATION);
                            this->descriptor.device_configuration_value = 
                                ((usb_uint16_t *)&request->std.wValue)->lower_byte;
                            ep->state = USB_DEVICE_EP_STATE_SEND_STATUS;
                            DF_DEBUG_NEXT_EVENT(USB_DEVICE_EP0_EP_STATE_SEND_STATUS);
                            ep->datain_packet_fifo.cbuf_push_packet_buf(NULL, 0);
                            usb_device_set_next_state(USB_DEVICE_STATE_CONFIGURED);
                            usb_device_configured();
                            send_ctrl_info = true;
                            break;
                        default:
                            usb_assert(0);
                            break;
                    }
                    break;
                case USB_INTERFACE:
                    usb_assert(0);
                    switch (bRequest) {
                        case USB_INTERFACE_GET_STATUS    :
                            break;
                        case USB_INTERFACE_CLEAR_FEATURE :
                            break;
                        case USB_INTERFACE_SET_FEATURE   :
                            break;
                        case USB_INTERFACE_GET_INTERFACE :
                            break;
                        case USB_INTERFACE_SET_INTERFACE :
                            break;
                        default:
                            usb_assert(0);
                            break;
                    }
                case USB_ENDPOINT:
                    usb_assert(0);
                    switch (bRequest) {
                        case USB_ENDPOINT_GET_STATUS    :
                            break;
                        case USB_ENDPOINT_CLEAR_FEATURE :
                            break;
                        case USB_ENDPOINT_SET_FEATURE   :
                            break;
                        case USB_ENDPOINT_SYNCH_FRAME   :
                            break;
                        default:
                            usb_assert(0);
                            break;
                    }
                case USB_OTHER:
                default:
                    usb_assert(0);
                    break;
            }
            break;
        case USB_CLASS_REQUEST:
        case USB_VENDOR_REQUEST:
        case USB_RESERVED_REQUEST:
                send_ctrl_info = true;
                if (core_handlers && core_handlers->setup_request) {
                cbuf_packet_item_t * datain_packet = ep->datain_packet_fifo.cbuf_get_next_fill_packet();
                core_handlers->setup_request(request, ep_num, datain_packet);
                if (datain_packet->len) {
                    ep->state = USB_DEVICE_EP_STATE_RECEIVE_STATUS;
                } else {
                    ep->state = USB_DEVICE_EP_STATE_SEND_STATUS;
                }
            }
            break;
        default:
            usb_assert(0);
            break;
    }
    if (send_ctrl_info) {
        usb_device_handle_schedule_data_in(ep_num);
    }
}

void usb_device_handle_t::usb_device_handle_sof(void)
{
    if (core_handlers && core_handlers->sof) {
        core_handlers->sof();
    }
}

void usb_device_handle_t::usb_device_handle_ep_datain_complete(uint8_t ep_num)
{
    usb_device_endpoint_descriptor_t * ep = usb_device_get_ep_descriptor(ep_num);
    usb_assert(ep);
    ep->num_datain++;
    usb_assert(ep->datain_state==USB_DEVICE_EP_TRANSACTION_SCHEDULED);
    ep->datain_state = USB_DEVICE_EP_TRANSACTION_IDLE;
    DF_DEBUG_NEXT_EVENT(USB_DEVICE_INEP_TRANSACTION_IDLE);

    cbuf_packet_item_t * packet_item = NULL;
    if (ep_num==0) {
        switch (ep->state) {
            case USB_DEVICE_EP_STATE_IDLE:
                /* Scheudle reception of a setup packet of 8 bytes */
                usb_device_handle_schedule_data_out(ep_num, 8);
                break;

            case USB_DEVICE_EP_STATE_RECEIVE_STATUS:
                /* Reset the state */
                ep->state = USB_DEVICE_EP_STATE_IDLE;
                DF_DEBUG_NEXT_EVENT(USB_DEVICE_EP0_EP_STATE_IDLE);
                /* Schedule a zero length packet to be received */
                usb_device_handle_schedule_data_out(ep_num, 0);
                break;

            case USB_DEVICE_EP_STATE_SEND_STATUS:
                ep->state = USB_DEVICE_EP_STATE_IDLE;            
                break;

            default:
                usb_assert(0);
        }
    } else {
        if (core_handlers && core_handlers->ep_datain_complete) {
            core_handlers->ep_datain_complete(ep_num | 0x80);
        }
    }
}

void usb_device_handle_t::usb_device_handle_ep_dataout_complete(uint8_t ep_num, uint8_t * buffer,
    uint8_t buffer_len)
{
    usb_device_endpoint_descriptor_t * ep = usb_device_get_ep_descriptor(ep_num);
    usb_assert(ep);
    usb_assert(ep->dataout_state==USB_DEVICE_EP_TRANSACTION_SCHEDULED);
    ep->dataout_state = USB_DEVICE_EP_TRANSACTION_IDLE;
    DF_DEBUG_NEXT_EVENT(USB_DEVICE_OUTEP_TRANSACTION_IDLE);

    cbuf_packet_item_t * packet_item = NULL;
    ep->num_dataout++;
    if (ep_num==0) {
        switch (ep->state) {
            case USB_DEVICE_EP_STATE_IDLE:
                DF_DEBUG_NEXT_EVENT(USB_DEVICE_SCHEDULE_NEXT_SETUP_RECEPTION);
                /* Scheudle reception of a setup packet of 8 bytes */
                usb_device_handle_schedule_data_out(ep_num, 8, true);
                break;

            case USB_DEVICE_EP_STATE_RECEIVE_STATUS:
                ep->state = USB_DEVICE_EP_STATE_IDLE;
                break;

            case USB_DEVICE_EP_STATE_SEND_STATUS:
                /* Reset the state */
                ep->state = USB_DEVICE_EP_STATE_IDLE;
                DF_DEBUG_NEXT_EVENT(USB_DEVICE_EP0_EP_STATE_IDLE);
                /* Schedule the next data-out transfer size */
                ep->datain_packet_fifo.cbuf_push_packet_buf(NULL, 0);
                usb_device_handle_schedule_data_in(ep_num);
                break;
            
            default:
                usb_assert(0);
        }
    } else {
        if (core_handlers && core_handlers->ep_dataout_complete) {
            cbuf_packet_item_t * packet_item = ep->dataout_packet_fifo.cbuf_get_last_packet();
            core_handlers->ep_dataout_complete(ep_num, packet_item);
        }
    }
}

void usb_device_handle_t::usb_device_handle_schedule_data_in(uint8_t ep_num)
{
    usb_device_endpoint_descriptor_t * ep = usb_device_get_ep_descriptor(ep_num);
    if (hw_handlers && hw_handlers->schedule_data_in) {
        if (ep->datain_state==USB_DEVICE_EP_TRANSACTION_IDLE) {
            DF_DEBUG_NEXT_EVENT(USB_DEVICE_INEP_TRANSACTION_SCHEDULED);
            ep->datain_state = USB_DEVICE_EP_TRANSACTION_SCHEDULED;
            hw_handlers->schedule_data_in(ep_num);
        } else {
            usb_assert(0);
        }
    } else {
        usb_assert(0);
    }
}

void usb_device_handle_t::usb_device_handle_schedule_data_out(uint8_t ep_num, uint8_t size, bool is_setup)
{
    usb_device_endpoint_descriptor_t * ep = usb_device_get_ep_descriptor(ep_num);
    if (hw_handlers && hw_handlers->schedule_data_out) {
        if (ep->dataout_state==USB_DEVICE_EP_TRANSACTION_IDLE) {
            if (is_setup) {
                ep->dataout_state = USB_DEVICE_EP_TRANSACTION_IDLE;
            } else {
                DF_DEBUG_NEXT_EVENT(USB_DEVICE_OUTEP_TRANSACTION_SCHEDULED);
                ep->dataout_state = USB_DEVICE_EP_TRANSACTION_SCHEDULED;
            }
            hw_handlers->schedule_data_out(ep_num, size);
        } else {
            usb_assert(0);
        }
    } else {
        usb_assert(0);
    }
}

void usb_device_handle_t::usb_device_start_dataout_transfer(uint8_t ep_num, uint8_t size)
{
    usb_assert(ep_num!=0);
    usb_device_endpoint_descriptor_t * ep = usb_device_get_ep_descriptor(ep_num);
    usb_device_handle_schedule_data_out(ep_num, size);
}

void usb_device_handle_t::usb_device_start_datain_transfer(uint8_t ep_num, uint8_t * buffer, uint8_t buffer_len)
{
    usb_assert(ep_num!=0);
    bool status;
    usb_device_endpoint_descriptor_t * ep = usb_device_get_ep_descriptor(ep_num);
    status = ep->datain_packet_fifo.cbuf_push_packet_buf(buffer, buffer_len);
    usb_assert(status);
    usb_device_handle_schedule_data_in(ep_num);
}


usb_device_endpoint_descriptor_t * usb_device_handle_t::usb_device_get_ep_descriptor(uint8_t ep_num)
{
    ep_num &= 0x7F;
    usb_assert(eps[ep_num].datain_max_packet_size);
    usb_assert(eps[ep_num].dataout_max_packet_size);
    return &eps[ep_num];
}

uint8_t usb_device_handle_t::usb_device_get_address()
{
    return descriptor.device_addr;
}


void usb_device_handle_t::usb_device_init(void)
{
    if (state_handlers && state_handlers->init) {
        state_handlers->init();
    }
}

void usb_device_handle_t::usb_device_reset(void)
{
    descriptor.device_addr = 0;
    descriptor.device_configuration_value = 0;
    prev_state      = USB_DEVICE_STATE_DEFAULT;
    curr_state      = USB_DEVICE_STATE_DEFAULT;
    eps[0].state    = USB_DEVICE_EP_STATE_IDLE;
    eps[0].datain_state    = USB_DEVICE_EP_TRANSACTION_IDLE;
    eps[0].dataout_state    = USB_DEVICE_EP_TRANSACTION_IDLE;
    hw_handlers->set_address(0x00);
    if (state_handlers && state_handlers->reset) {
        state_handlers->reset();
    }
}

void usb_device_handle_t::usb_device_configured(void)
{
    if (state_handlers && state_handlers->configured) {
        state_handlers->configured();
    }
}

void usb_device_handle_t::usb_device_suspended(void)
{
    if (state_handlers && state_handlers->suspended) {
        state_handlers->suspended();
    }
}

void usb_device_handle_t::usb_device_resumed(void)
{
    if (state_handlers && state_handlers->resumed) {
        state_handlers->resumed();
    }
}

void usb_device_handle_t::usb_device_connected(void)
{
    if (state_handlers && state_handlers->connected) {
        state_handlers->connected();
    }
}

void usb_device_handle_t::usb_device_disconnected(void)
{
    if (state_handlers && state_handlers->disconnected) {
        state_handlers->disconnected();
    }
}

void usb_device_handle_t::usb_device_set_next_state(usb_device_state_enum_t state)
{
    prev_state = curr_state;
    curr_state = state;
}

usb_device_state_enum_t usb_device_handle_t::usb_device_get_curr_state(void)
{
    return curr_state;
}


uint8_t usb_device_get_address()
{
    df_assert(g_usb_device_handle);
    g_usb_device_handle->usb_device_get_address();
}

void usb_device_start_dataout_transfer(uint8_t ep_num, uint8_t size)
{
    df_assert(g_usb_device_handle);
    g_usb_device_handle->usb_device_start_dataout_transfer(ep_num, size);
}

void usb_device_start_datain_transfer(uint8_t ep_num, uint8_t * buffer, uint8_t buffer_len)
{
    df_assert(g_usb_device_handle);
    g_usb_device_handle->usb_device_start_datain_transfer(ep_num, buffer, buffer_len);
}
