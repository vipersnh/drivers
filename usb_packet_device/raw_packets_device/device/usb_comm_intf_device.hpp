#ifndef _USB_COMM_INTF_DEVICE_HPP_
#define _USB_COMM_INTF_DEVICE_HPP_

class usb_comm_t
{
    public:
        usb_comm_t(void);
        /* Statistics */
        uint32_t num_packets_received;
        uint32_t num_packets_sent;

        static usb_comm_t * get_instance();
        void usb_comm_init();
        void usb_comm_intf_start();
        uint8_t usb_comm_get_num_packets_received();
        void usb_comm_send_deque_packet(usb_comm_packet_t *packet);
        void usb_comm_send_enque_packet(usb_comm_packet_t *packet);
    
        void usb_comm_recv_deque_packet(usb_comm_packet_t *packet);
        void usb_comm_recv_enque_packet(usb_comm_packet_t *packet);

        void usb_comm_send_tx_fifo_stats();
        void usb_comm_send_rx_fifo_stats();
        void usb_comm_send_tx_next_packet();
        bool          send_complete;

    protected:
        cbuf_packet_t tx_fifo;
        cbuf_packet_t rx_fifo;

    private:
        static usb_comm_t * instance;

};

/* Functions called from USB ISR for indication of transfers */
extern void usb_comm_handler_intf_start(void);
extern void usb_comm_handler_ep_dataout_complete(uint8_t ep_num, cbuf_packet_item_t * packet_item);
extern void usb_comm_handler_ep_datain_complete(uint8_t ep_num);

/* Functions called from UBS COMM INTF for starting USB transfers */
extern void usb_comm_callback_ep_activate(uint8_t ep_num);
extern void usb_comm_callback_ep_dataout_start(uint8_t ep_num, uint8_t buffer_len);
extern void usb_comm_callback_ep_datain_start(uint8_t ep_num, uint8_t * buffer, uint8_t buffer_len);

extern usb_comm_t * g_usb_comm_handle;



#endif
