#include "df_base.h"
#include "cbuf_packet.h"
#include "usb_comm_intf.hpp"
#include "usb_comm_intf_device.hpp"
#include "usb_comm_intf_device_ut.hpp"

void usb_comm_intf_ut_task(void *param)
{
    usb_comm_t * handle = usb_comm_t::get_instance();
    usb_comm_packet_t   packet;
    do {
        if (handle->usb_comm_get_num_packets_received()) {
            handle->usb_comm_recv_deque_packet(&packet);
            handle->usb_comm_send_enque_packet(&packet);
        } else {
            df_usleep(10);
        }
    } while (1);
}
