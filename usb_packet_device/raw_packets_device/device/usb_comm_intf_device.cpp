#include <stdint.h>
#include <stdio.h>
#include "df_base.h"
#include "cbuf_packet.h"
#include "usb_comm_intf.hpp"
#include "usb_comm_intf_device.hpp"



usb_comm_t * g_usb_comm_handle;

usb_comm_t * usb_comm_t::instance;


usb_comm_t::usb_comm_t(void)
{
    instance = NULL;
    send_complete = true;
    num_packets_received = num_packets_sent = 0;
    return;
}

usb_comm_t * usb_comm_t::get_instance()
{
    if (instance==NULL) {
        instance = new usb_comm_t();
    } else {
        /* TODO */
    }
    g_usb_comm_handle = instance;
    return instance;
}

void usb_comm_t::usb_comm_init()
{
    this->tx_fifo.cbuf_init(USB_COMM_INTF_MAX_NUM_PACKETS);
    this->rx_fifo.cbuf_init(USB_COMM_INTF_MAX_NUM_PACKETS);

    this->tx_fifo.cbuf_setup_packet_length(USB_COMM_INTF_MAX_PACKET_SIZE);
    this->rx_fifo.cbuf_setup_packet_length(USB_COMM_INTF_MAX_PACKET_SIZE);

    this->send_complete = true;
}

void usb_comm_t::usb_comm_intf_start()
{
    usb_comm_callback_ep_activate(USB_COMM_DATAOUT_EP);
    usb_comm_callback_ep_activate(USB_COMM_DATAIN_EP);
    /* Start reception of next command packet */
    usb_comm_callback_ep_dataout_start(USB_COMM_DATAOUT_EP, USB_COMM_INTF_MAX_PACKET_SIZE);
}

uint8_t usb_comm_t::usb_comm_get_num_packets_received()
{
    return this->rx_fifo.cbuf_get_num_elem();
}

void usb_comm_t::usb_comm_send_deque_packet(usb_comm_packet_t * packet)
{
    uint16_t len;
    this->tx_fifo.cbuf_pop_packet_buf((uint8_t*)packet, &len, sizeof(*packet));   
}

void usb_comm_t::usb_comm_send_enque_packet(usb_comm_packet_t * packet)
{
    this->tx_fifo.cbuf_push_packet_buf((uint8_t*)packet, sizeof(*packet));
}

void usb_comm_t::usb_comm_recv_deque_packet(usb_comm_packet_t * packet)
{
    uint16_t len;
    this->rx_fifo.cbuf_pop_packet_buf((uint8_t*)packet, &len, sizeof(*packet));   
}

void usb_comm_t::usb_comm_recv_enque_packet(usb_comm_packet_t * packet)
{
    this->rx_fifo.cbuf_push_packet_buf((uint8_t*)packet, sizeof(*packet));
}

void usb_comm_t::usb_comm_send_tx_fifo_stats()
{
    usb_comm_cmd_resp_t cmd_resp;
    cmd_resp.cmd = USB_COMM_CMD_GET_TX_FIFO_STATS;
    cmd_resp.data.tx_fifo_stats.num_filled = this->tx_fifo.cbuf_get_num_elem();
    cmd_resp.data.tx_fifo_stats.num_free = this->tx_fifo.cbuf_get_num_available();
    usb_comm_callback_ep_datain_start(USB_COMM_DATAIN_EP, (uint8_t*)&cmd_resp, sizeof(cmd_resp));
}

void usb_comm_t::usb_comm_send_rx_fifo_stats()
{
    usb_comm_cmd_resp_t cmd_resp;
    cmd_resp.cmd = USB_COMM_CMD_GET_RX_FIFO_STATS;
    cmd_resp.data.rx_fifo_stats.num_filled = this->rx_fifo.cbuf_get_num_elem();
    cmd_resp.data.rx_fifo_stats.num_free = this->rx_fifo.cbuf_get_num_available();
    usb_comm_callback_ep_datain_start(USB_COMM_DATAIN_EP, (uint8_t*)&cmd_resp, sizeof(cmd_resp));
}

void usb_comm_t::usb_comm_send_tx_next_packet()
{
    df_assert(this->tx_fifo.cbuf_get_num_elem());
    cbuf_packet_item_t * packet_item = this->tx_fifo.cbuf_get_first_packet();
    usb_comm_callback_ep_datain_start(USB_COMM_DATAIN_EP, (uint8_t*)packet_item->packet_buf, packet_item->len);
    this->tx_fifo.cbuf_pop_packet_buf(NULL, NULL, 0);
}

void usb_comm_handler_intf_start(void)
{
    g_usb_comm_handle->usb_comm_intf_start();
}

void usb_comm_handler_ep_dataout_complete(uint8_t ep_num, cbuf_packet_item_t * packet_item)
{
    g_usb_comm_handle->num_packets_received++;
    df_assert(ep_num==USB_COMM_DATAOUT_EP);
    df_assert(packet_item->len==USB_COMM_INTF_MAX_PACKET_SIZE);
    usb_comm_packet_t * packet = (usb_comm_packet_t*)packet_item->packet_buf;
    switch (packet->hdr.pkt_type) {
        case USB_COMM_COMMAND:
            {   
                df_assert(packet->hdr.length==(sizeof(usb_comm_intf_hdr_t)+sizeof(usb_comm_cmd_t)));
                usb_comm_cmd_t * cmd = (usb_comm_cmd_t *)packet->data;
                switch (cmd->cmd) {
                    case USB_COMM_CMD_GET_TX_FIFO_STATS:
                        g_usb_comm_handle->usb_comm_send_tx_fifo_stats();
                        break;
                    case USB_COMM_CMD_GET_RX_FIFO_STATS:
                        g_usb_comm_handle->usb_comm_send_rx_fifo_stats();
                        break;
                    case USB_COMM_CMD_GET_TX_NEXT_PACKET:
                        g_usb_comm_handle->usb_comm_send_tx_next_packet();
                        break;
                    default:
                        df_assert(0);
                }
                df_assert(g_usb_comm_handle->send_complete);
                g_usb_comm_handle->send_complete = false;
            }
            break;
        case USB_COMM_DATA:
            /* Received a dataout packet from host */
            df_assert(packet->hdr.length<=(sizeof(usb_comm_packet_t)));
            g_usb_comm_handle->usb_comm_recv_enque_packet((usb_comm_packet_t *)packet_item->packet_buf);
            break;
        default:
            df_assert(0);
    }

    /* Start reception of next command packet */
    usb_comm_callback_ep_dataout_start(USB_COMM_DATAOUT_EP, USB_COMM_INTF_MAX_PACKET_SIZE);
}

void usb_comm_handler_ep_datain_complete(uint8_t ep_num)
{
    g_usb_comm_handle->num_packets_sent++;
    df_assert(ep_num==USB_COMM_DATAIN_EP);
    df_assert(!g_usb_comm_handle->send_complete);
    g_usb_comm_handle->send_complete = true;;
}
