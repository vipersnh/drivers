#include <stdint.h>
#include "usb_core_common.hpp"

bool g_usb_assert_end;

void usb_assert(bool flag)
{
    if (!flag) {
        g_usb_assert_end = false;
        if (g_usb_assert_callback) {
            g_usb_assert_callback();
        }
        while (!g_usb_assert_end);
    }
}


