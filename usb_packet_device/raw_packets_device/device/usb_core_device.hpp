#ifndef __USB_CORE_DEVICE_HPP_
#define __USB_CORE_DEVICE_HPP_

#define USB_DEVICE_MAX_ENDPOINTS     (4)


typedef enum {
    USB_HOST_TO_DEVICE = 0,
    USB_DEVICE_TO_HOST = 1,
} usb_request_data_direction_enum_t;

typedef enum {
    USB_STANDARD_REQUEST = 0,
    USB_CLASS_REQUEST    = 1,
    USB_VENDOR_REQUEST   = 2,
    USB_RESERVED_REQUEST = 3,
} usb_request_type_enum_t;

typedef enum {
    USB_DEVICE    = 0,
    USB_INTERFACE = 1,
    USB_ENDPOINT  = 2,
    USB_OTHER     = 3,
} usb_request_recipient_enum_t;

typedef enum {
    USB_DEVICE_GET_STATUS        = 0x00,
    USB_DEVICE_CLEAR_FEATURE     = 0x01,
    USB_DEVICE_SET_FEATURE       = 0x03,
    USB_DEVICE_SET_ADDRESS       = 0x05,
    USB_DEVICE_GET_DESCRIPTOR    = 0x06,
    USB_DEVICE_SET_DESCRIPTOR    = 0x07,
    USB_DEVICE_GET_CONFIGURATION = 0x08,
    USB_DEVICE_SET_CONFIGURATION = 0x09,
} usb_standard_device_request_enum_t;

typedef enum {
    USB_INTERFACE_GET_STATUS    = 0x00,
    USB_INTERFACE_CLEAR_FEATURE = 0x01,
    USB_INTERFACE_SET_FEATURE   = 0x03,
    USB_INTERFACE_GET_INTERFACE = 0x0A,
    USB_INTERFACE_SET_INTERFACE = 0x11,
} usb_standard_interface_request_enum_t;

typedef enum {
    USB_ENDPOINT_GET_STATUS    = 0x00,
    USB_ENDPOINT_CLEAR_FEATURE = 0x01,
    USB_ENDPOINT_SET_FEATURE   = 0x03,
    USB_ENDPOINT_SYNCH_FRAME   = 0x12,
} usb_standard_endpoint_request_enum_t;

typedef enum {
    USB_DEVICE_DESCRIPTOR        = 0x01,
    USB_CONFIGURATION_DESCRIPTOR = 0x02,
    USB_STRING_DESCRIPTOR        = 0x03,
    USB_INTERFACE_DESCRIPTOR     = 0x04,
    USB_ENDPOINT_DESCRIPTOR      = 0x05,
    USB_DEBUG_DESCRIPTOR         = 0x0A,
} usb_standard_descriptor_types_enum_t;

typedef struct  {
    struct {
        uint8_t  bmRequest;
        uint8_t  bRequest;
        uint16_t wValue;
        uint16_t wIndex;
        uint16_t wLength;
    } _PACKED_ std;
} usb_setup_packet_t;

typedef struct {
    void (*activate_dataout)(uint8_t ep_num);  /* Should activate out for the ep_num */
    void (*set_address)(uint8_t addr);
    void (*schedule_data_in)(uint8_t ep_num);
    void (*schedule_data_out)(uint8_t ep_num, uint8_t size);
} usb_device_hardware_handlers_t;

typedef struct {
    void (*sof)(void);
    void (*setup_request)(usb_setup_packet_t * request, uint8_t ep_num, cbuf_packet_item_t * datain_packet);
    void (*ep_datain_complete)(uint8_t ep_num); /* Non-zero endpoints */
    void (*ep_dataout_complete)(uint8_t ep_num, cbuf_packet_item_t * packet_item); /* Non-zero endpoints */
} usb_device_core_handlers_t;

typedef struct {
    void (*init)(void);
    void (*reset)(void);
    void (*configured)(void);
    void (*suspended)(void);
    void (*resumed)(void);
    void (*connected)(void);
    void (*disconnected)(void);
} usb_device_state_handlers_t;

typedef enum {
    USB_DEVICE_STATE_SUSPENDED,
    USB_DEVICE_STATE_POWERED,
    USB_DEVICE_STATE_DEFAULT,
    USB_DEVICE_STATE_ADDRESSED,
    USB_DEVICE_STATE_CONFIGURED,
} usb_device_state_enum_t;

class usb_device_handle_t {
    public:
        /* Initialization functions */
        usb_device_handle_t(void);
        static usb_device_handle_t * get_instance();
        void usb_device_handle_init();
        void usb_device_setup_descriptors(usb_device_descriptor_t * dev_descriptor, 
            usb_device_configuration_descriptor_t *conf_descriptor, uint8_t num_configurations, 
            usb_device_endpoint_descriptor_t * control_ep, usb_device_string_descriptor_zero_t * str_descriptor0,
            usb_device_string_descriptor_t * str_descriptors, uint8_t num_str_descriptors);
        void usb_device_setup_core_handlers(usb_device_core_handlers_t * core_handlers);
        void usb_device_setup_hardware_handlers(usb_device_hardware_handlers_t * core_handlers);
        void usb_device_setup_state_handlers(usb_device_state_handlers_t * state_handlers);


        /* USB ISR functions, must only be called from USB ISR */
        void usb_device_init(void);
        void usb_device_reset(void);
        void usb_device_suspended(void);
        void usb_device_resumed(void);
        void usb_device_connected(void);
        void usb_device_disconnected(void);
        void usb_device_handle_setup_request(usb_setup_packet_t * request, uint8_t ep_num);
        void usb_device_handle_sof(void);
        void usb_device_handle_ep_datain_complete(uint8_t ep_num);
        void usb_device_handle_ep_dataout_complete(uint8_t ep_num, uint8_t * buffer, uint8_t buffer_len);
        usb_device_endpoint_descriptor_t * usb_device_get_ep_descriptor(uint8_t ep_num);
    
        /* Public functions which can be called from class/higher layer protocol */
        uint8_t usb_device_get_address();
        void usb_device_activate_endpoint(uint8_t ep_num);
        void usb_device_start_dataout_transfer(uint8_t ep_num, uint8_t size);
        void usb_device_start_datain_transfer(uint8_t ep_num, uint8_t * buffer, uint8_t buffer_len);

    private:
        /* Internal functions */
        void usb_device_handle_schedule_data_in(uint8_t ep_num);
        void usb_device_handle_schedule_data_out(uint8_t ep_num, uint8_t size, bool is_setup=false);
        void usb_device_configured(void);

    private:
        usb_device_state_enum_t            prev_state;
        usb_device_state_enum_t            curr_state;
        usb_device_complete_descriptor_t   descriptor;
        usb_device_hardware_handlers_t   * hw_handlers;
        usb_device_core_handlers_t       * core_handlers;
        usb_device_state_handlers_t      * state_handlers;
        usb_device_endpoint_descriptor_t * control_ep;  /* Points to eps[0] */
        usb_device_endpoint_descriptor_t   eps[USB_DEVICE_MAX_ENDPOINTS];

        void usb_device_set_next_state(usb_device_state_enum_t);
        usb_device_state_enum_t usb_device_get_curr_state(void);

    protected:
        static usb_device_handle_t * instance;

    private :   /* Statistics */
        uint32_t num_setup_packets_handled;
        uint32_t num_device_descriptor_reqs;
        uint32_t num_set_address_reqs;
        uint32_t num_ep_flushing;
        
};

extern usb_device_handle_t * g_usb_device_handle;

extern uint8_t usb_device_get_address();
extern void usb_device_start_dataout_transfer(uint8_t ep_num, uint8_t size);
extern void usb_device_start_datain_transfer(uint8_t ep_num, uint8_t * buffer, uint8_t buffer_len);

#endif
