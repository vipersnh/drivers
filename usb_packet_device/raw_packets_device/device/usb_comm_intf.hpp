#ifndef _USB_COMM_INTF_HPP_
#define _USB_COMM_INTF_HPP_

#define USB_COMM_INTF_MAX_NUM_PACKETS   10
#define USB_COMM_INTF_MAX_PACKET_SIZE   64

#define USB_COMM_DATAIN_EP              0x81
#define USB_COMM_DATAOUT_EP             0x01

typedef enum {
    USB_COMM_DATA    = 0,
    USB_COMM_COMMAND = 1,
} usb_comm_pkt_type_enum_t;

typedef struct {
    uint8_t pkt_type: 1;
    uint8_t ch_id   : 3;
    uint8_t length  : 4;
} _PACKED_ usb_comm_intf_hdr_t;

typedef struct {
    usb_comm_intf_hdr_t     hdr;
    uint8_t                 data[USB_COMM_INTF_MAX_PACKET_SIZE - sizeof(usb_comm_intf_hdr_t)];
} _PACKED_ usb_comm_packet_t;

typedef enum {
    USB_COMM_CMD_GET_TX_FIFO_STATS  = 0,
    USB_COMM_CMD_GET_RX_FIFO_STATS  = 1,
    USB_COMM_CMD_GET_TX_NEXT_PACKET = 2,
} usb_comm_cmd_enum_t;

typedef struct {
    uint32_t cmd;
} _PACKED_ usb_comm_cmd_t;


typedef struct {
    uint32_t cmd;
    union {
        struct {
            uint8_t num_filled;
            uint8_t num_free;
        } tx_fifo_stats;

        struct {
            uint8_t num_filled;
            uint8_t num_free;
        } rx_fifo_stats;
    } data;
} _PACKED_ usb_comm_cmd_resp_t;

#endif
