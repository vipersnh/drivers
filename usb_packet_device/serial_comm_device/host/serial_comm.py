import sys
sys.path.insert(0, "../../raw_packets_device/host")

from PyQt4.QtCore import QObject, pyqtSignal, QThread, QTimer, QMutex, Qt, qDebug, pyqtRemoveInputHook
from PyQt4.QtGui import QTextEdit, QTextCursor, QApplication
from pdb import set_trace
from usb_comm_intf_host import usb_comm_t

class serial_usb_comm_device_t (QObject):
    signal_serial_data_received = pyqtSignal()
    MAX_PACKET_DATA_LEN = 50

    def __init__(self, idVendor, idProduct, parent=None):
        super(QObject, self).__init__(parent)
        self.poll_timer = QTimer(self)
        self.poll_timer.timeout.connect(self.slot_serial_usb_poll_for_data, Qt.DirectConnection)
        self.usb_comm = usb_comm_t()
        self.recv_data_mutex = QMutex()
        self.send_data_mutex = QMutex()
        self.recv_data_buffer = bytearray()
        self.send_data_buffer = bytearray()
        self.usb_comm.usb_comm_init(idVendor, idProduct)

    def start_usb_poll(self, ms_interval=1000):
        self.poll_timer.stop()
        self.poll_timer.setInterval(ms_interval)
        self.poll_timer.start()

    def slot_serial_usb_poll_for_data(self):
        if self.usb_comm.usb_comm_connected():
            [tx_num_filled, tx_num_free] = self.usb_comm.usb_comm_get_tx_fifo_stats()
            [rx_num_filled, rx_num_free] = self.usb_comm.usb_comm_get_rx_fifo_stats()
            if tx_num_filled:
                self.recv_data_mutex.lock()
                for i in range(tx_num_filled):
                    [packet_hdr, packet_data] = self.usb_comm.usb_comm_receive_packet()
                    self.recv_data_buffer.append(packet_data)
                self.recv_data_mutex.unlock()
                self.signal_serial_data_received.emit()
            if rx_num_free and len(self.send_data_buffer):
                self.send_data_mutex.lock()
                for i in range(rx_num_free):
                    if len(self.send_data_buffer):
                        if len(self.send_data_buffer)>MAX_PACKET_DATA_LEN:
                            self.usb_comm.usb_comm_send_packet(self.send_data_buffer.pop[0:MAX_PACKET_DATA_LEN-1], 0x01)
                            self.send_data_buffer.pop[0:MAX_PACKET_DATA_LEN-1] = 0
                        else:
                            self.usb_comm.usb_comm_send_packet(self.send_data_buffer.pop[:], 0x01)
                            self.send_data_buffer.pop[:] = 0
                    else:
                        break
                self.send_data_mutex.unlock()
        else:
            # For debug purpose
            self.recv_data_mutex.lock()
            self.recv_data_buffer += "abcd".encode()
            self.recv_data_mutex.unlock()
            #print("Emitting signal_serial_data_received")
            self.signal_serial_data_received.emit()
    
    def slot_serial_usb_send_data(self, data):
        self.serial_usb_send_data(data)

    def serial_usb_send_data(self, data):
        self.send_data_mutex.lock()
        self.send_data_buffer.append(data)
        self.send_data_mutex.unlock()

    def serial_usb_recv_data(self):
        if len(self.recv_data_buffer):
            self.recv_data_mutex.lock()
            ret_bytearray =  self.recv_data_buffer[:]
            self.recv_data_buffer.clear()
            self.recv_data_mutex.unlock()
            return ret_bytearray
        else:
            return None

class serial_text_window_t(QTextEdit):
    def __init__(self, idVendor, idProduct, parent=None):
        super(QTextEdit, self).__init__(parent)
        self.setCursorWidth(0)
        self.serial_device = serial_usb_comm_device_t(idVendor, idProduct)
        self.serial_device.signal_serial_data_received.connect(self.slot_recv_serial_data)
        self.serial_device.start_usb_poll()

    def keyPressEvent(self, ev):
        ev.accept()
        text = ev.text()
        if len(text):
            self.moveCursor(QTextCursor.End)
            self.textCursor().insertText(text)
            self.serial_device.slot_serial_usb_send_data(text.encode()[0])

    def slot_recv_serial_data(self):
        text = self.serial_device.serial_usb_recv_data().decode()
        self.insertPlainText(text)

if __name__ == '__main__':    
    idVendor=0xf0f0
    idProduct=0xa0b0

    pyqtRemoveInputHook()
    app = QApplication(sys.argv)
    serial_editor = serial_text_window_t(idVendor, idProduct)
    serial_editor.resize(800, 600)
    serial_editor.show()
    sys.exit(app.exec_())



